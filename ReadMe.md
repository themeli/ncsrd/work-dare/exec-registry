# Exec-registry

## Update db

1. python manage.py makemigrations
2. python manage.py migrate
3. python manage.py migrate --run-syncdb
4. python manage.py createsuperuser
5. python manage.py runserver 
6. python manage.py generateschema > openapi-schema.yml
7. export DJANGO_SETTINGS_MODULE=exec_registry.settings
8. python manage.py collectstatic

## Generating an OpenAPI Schema

* pip install pyyaml
* ./manage.py generateschema > openapi-schema.yml
