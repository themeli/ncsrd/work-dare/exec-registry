Testing Exec-Registry API
=========================

.. toctree::
    :caption: Testing

.. automodule:: registry.tests
   :members: