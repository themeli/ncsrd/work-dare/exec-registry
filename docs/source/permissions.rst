Permissions for the RESTful API
===============================

.. toctree::
    :caption: Permissions

.. automodule:: registry.permissions
   :members: