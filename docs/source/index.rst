.. Exec-Registry documentation master file, created by
   sphinx-quickstart on Mon Jan 27 13:08:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Exec-Registry's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   apps
   models
   views
   utils
   tests

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
