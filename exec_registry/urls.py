from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from exec_registry import settings
from registry.views import *

schema_view = get_swagger_view(title='Exec-api Registry')

router = routers.DefaultRouter()
router.register(r'runs', RunView, basename="runs")
router.register(r'experiments', ExperimentView, basename="experiments")
router.register(r'experiment-groups', ExperimentGroupView, basename="experiment-groups")
router.register(r'uploads', UploadView, basename="uploads")
router.register(r'downloads', DownloadView, basename="downloads")
router.register(r'accounts', LoginView, basename="accounts")

urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^', include(router.urls)),
                  url(r'^docs/$', schema_view)
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

admin.site.site_header = 'Exec-api Registry Admin'
admin.site.site_title = "Exec-api Registry"
