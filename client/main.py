import json
from os import getcwd
from os.path import join

import exec_registry_client as hf

login_hostname = "https://testbed.project-dare.eu/dare-login"
# hostname = "https://testbed.project-dare.eu/exec-registry"
hostname = "http://localhost:8000"

experiment_group_name = "Group1"
new_experiment_group_name = "Group2"
experiment_name = "Experiment1"
new_experiment_name = "Experiment2"
run_name = "Run1"
new_run_name = "Run2"
kind = "official"
run_filename = "logs.txt"

upload_folder = "d4p-input"
upload_file = "input.json"
new_upload_file = "inputs.json"
new_upload_folder = "my-split-merge"
dataset_name = "test"

test_path_to_files = join(getcwd(), "example_files")


def test_experiment_group(token):
    # create
    response = hf.create_experiment_group(hostname=hostname, token=token)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # update
    response = hf.update_experiment_group(hostname=hostname, token=token, experiment_group_name=experiment_group_name,
                                          new_experiment_group_name=new_experiment_group_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # delete
    response = hf.delete_experiment_group(hostname=hostname, token=token, experiment_group_name=experiment_group_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by user
    response = hf.get_experiment_groups_by_user(hostname=hostname, token=token)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by name
    response = hf.get_experiment_group_by_name(hostname=hostname, token=token,
                                               experiment_group_name=experiment_group_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])


def test_experiment(token):
    # create
    response = hf.create_experiment(hostname=hostname, token=token)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # update
    response = hf.update_experiment(hostname=hostname, token=token, experiment_name=experiment_name,
                                    new_experiment_name=new_experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # delete
    response = hf.delete_experiment(hostname=hostname, token=token, experiment_name=experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by user
    response = hf.get_experiments_by_user(hostname=hostname, token=token)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by name
    response = hf.get_experiment_by_name(hostname=hostname, token=token, experiment_name=experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by group
    response = hf.get_experiment_by_group(hostname=hostname, token=token, experiment_group_name=experiment_group_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])


def test_run(token):
    # create
    response = hf.create_run(hostname=hostname, token=token, experiment_name=experiment_name, kind=kind)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # update
    response = hf.update_run(hostname=hostname, token=token, experiment_name=experiment_name, run_name=run_name,
                             new_run_name=new_run_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # delete
    response = hf.delete_run(hostname=hostname, token=token, run_name=run_name, experiment_name=experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by experiment
    response = hf.get_runs_by_experiment(hostname=hostname, token=token, experiment_name=experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by name
    response = hf.get_run_by_name(hostname=hostname, token=token, run_name=run_name, experiment_name=experiment_name)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])

    # get by user
    response = hf.get_runs_by_user(hostname=hostname, token=token)
    if response[0] != 200:
        print("Error {}".format(response[1]))
    else:
        print(response[1])


def test_upload(token):
    # create upload
    response = hf.create_upload(hostname=hostname, token=token, filename=upload_file, path_to_file=test_path_to_files,
                                remote_path=upload_folder, dataset_name=dataset_name)
    if response[0] != 200:
        print("Error! {}".format(response[1]))
    else:
        upload = json.loads(response[1])
        print(upload)

    # update upload
    response = hf.update_upload(hostname=hostname, token=token, old_filename=upload_file, old_path=upload_folder,
                                new_filename=new_upload_file, new_folder_name=new_upload_folder,
                                path_to_file=test_path_to_files)

    if response[0] != 200:
        print("Error! {}".format(response[1]))
    else:
        upload = json.loads(response[1])
        print(upload)
    # delete upload
    response = hf.delete_upload(hostname=hostname, token=token, filename=new_upload_file, folder=new_upload_folder)
    print(response)

    # get by user
    response = hf.get_upload_by_user(hostname=hostname, token=token)
    print(response)

    # get by folder
    response = hf.get_upload_by_folder(hostname=hostname, token=token, folder=upload_folder)
    print(response)


def test_download(token):
    # get full_path for runs
    download_kind = "experiments"
    response = hf.get_full_path(hostname, token, download_kind, experiment_name=experiment_name,
                                experiment_group_name=experiment_group_name, run_name=run_name,
                                upload_folder=None, upload_file=None)
    if response[0] != 200:
        print("Error: {}".format(response[1]))
    else:
        path = json.loads(response[1])["path"]
        print(path)
    # get full_path for uploads
    download_kind = "uploads"
    response = hf.get_full_path(hostname, token, download_kind, experiment_name=None, experiment_group_name=None,
                                run_name=None, upload_folder=upload_folder, upload_file=upload_file)
    if response[0] != 200:
        print("Error: {}".format(response[1]))
    else:
        path = json.loads(response[1])["path"]
        print(path)

    # download file
    response = hf.download_file(hostname=hostname, token=token, experiment_name=experiment_name, run_name=run_name,
                                filename=run_filename, experiment_group_name=experiment_group_name)
    print(response)


if __name__ == '__main__':
    credentials = hf.load_credentials()
    auth_response = hf.login(hostname=login_hostname, username=credentials["username"],
                             password=credentials["password"], requested_issuer=credentials["issuer"])
    if auth_response and type(auth_response) == dict:
        access_token = auth_response["access_token"]
        # test_experiment_group(token=access_token)
        test_experiment(token=access_token)
        # test_run(token=access_token)
        # test_upload(token=access_token)
        # test_download(token=access_token)
