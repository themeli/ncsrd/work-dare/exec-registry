from os.path import join, exists
from os import getcwd
import json
import yaml
import requests


def load_credentials():
    credentials_file = "credentials.yaml"
    example_credentials_file = "example_credentials.yaml"
    cred_file = join(getcwd(), credentials_file) if exists(join(getcwd(), credentials_file)) else \
        join(getcwd(), example_credentials_file)
    with open(cred_file, "r") as f:
        creds = yaml.safe_load(f.read())
        if not creds["issuer"]:
            creds["issuer"] = "dare"
        return creds


# **************************** Login *************************************
def login(hostname, username, password, requested_issuer):
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(r.text)
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"]}
    else:
        print("Could not authenticate user!")


# *************************** Experiment Group *****************************
def create_experiment_group(hostname, token, experiment_group_name=None, dare_platform=None):
    url = hostname + "/experiment-groups/"

    data = {
        "access_token": token
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.post(url, data=data)
    return response.status_code, response.text


def update_experiment_group(hostname, token, experiment_group_name, new_experiment_group_name, dare_platform=None):
    url = hostname + "/experiment-groups/update-exp-group/"

    data = {
        "access_token": token,
        "experiment_group_name": experiment_group_name,
        "new_experiment_group_name": new_experiment_group_name
    }

    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.post(url, data=data)
    return response.status_code, response.text


def delete_experiment_group(hostname, token, experiment_group_name, dare_platform=None):
    url = hostname + "/experiment-groups/delete-exp-group/"

    data = {
        "access_token": token,
        "experiment_group_name": experiment_group_name
    }

    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_experiment_groups_by_user(hostname, token, dare_platform=None):
    url = hostname + "/experiment-groups/byuser"

    params = {"access_token": token}

    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


def get_experiment_group_by_name(hostname, token, experiment_group_name, dare_platform=None):
    url = hostname + "/experiment-groups/byname"

    params = {
        "access_token": token,
        "experiment_group_name": experiment_group_name
    }

    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


# ***************************** Experiment *********************************
def create_experiment(hostname, token, experiment_name=None, experiment_group_name=None, dare_platform=None):
    url = hostname + "/experiments/"

    data = {
        "access_token": token
    }

    if experiment_name:
        data["experiment_name"] = experiment_name
    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.post(url, data=data)
    return response.status_code, response.text


def update_experiment(hostname, token, experiment_name, experiment_group_name=None, dare_platform=None,
                      new_experiment_group_name=None, new_experiment_name=None):
    url = hostname + "/experiments/update-exp/"

    data = {
        "access_token": token,
        "experiment_name": experiment_name
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform
    if new_experiment_group_name:
        data["new_experiment_group_name"] = new_experiment_group_name
    if new_experiment_name:
        data["new_experiment_name"] = new_experiment_name

    response = requests.post(url, data=data)
    return response.status_code, response.text


def delete_experiment(hostname, token, experiment_name, experiment_group_name=None, dare_platform=None):
    url = hostname + "/experiments/delete-experiment/"

    data = {
        "access_token": token,
        "experiment_name": experiment_name
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_experiments_by_user(hostname, token, experiment_group_name=None, dare_platform=None):
    url = hostname + "/experiments/byuser"

    params = {"access_token": token}

    if experiment_group_name:
        params["experiment_group_name"] = experiment_group_name
    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


def get_experiment_by_name(hostname, token, experiment_name, experiment_group_name=None, dare_platform=None):
    url = hostname + "/experiments/byname"

    params = {
        "access_token": token,
        "experiment_name": experiment_name
    }

    if experiment_group_name:
        params["experiment_group_name"] = experiment_group_name
    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


def get_experiment_by_group(hostname, token, experiment_group_name, dare_platform=None):
    url = hostname + "/experiments/bygroup"

    params = {
        "access_token": token,
        "experiment_group_name": experiment_group_name
    }

    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


# ********************************** Run ***********************************
def create_run(hostname, token, experiment_name, kind, run_name=None, experiment_group_name=None, dare_platform=None):
    url = hostname + "/runs/"

    data = {
        "access_token": token,
        "experiment_name": experiment_name,
        "kind": kind
    }
    if run_name:
        data["run_name"] = run_name
    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.post(url, data=data)
    return response.status_code, response.text


def update_run(hostname, token, experiment_name, run_name, experiment_group_name=None, dare_platform=None,
               new_run_name=None, new_experiment_name=None, new_experiment_group_name=None):
    url = hostname + "/runs/update-run/"

    data = {
        "access_token": token,
        "experiment_name": experiment_name,
        "run_name": run_name
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform
    if new_run_name:
        data["new_run_name"] = new_run_name
    if new_experiment_name:
        data["new_experiment_name"] = new_experiment_name
    if new_experiment_group_name:
        data["new_experiment_group_name"] = new_experiment_group_name

    response = requests.post(url, data=data)
    return response.status_code, response.text


def delete_run(hostname, token, run_name, experiment_name, experiment_group_name=None, dare_platform=None):
    url = hostname + "/runs/delete-run/"

    data = {
        "access_token": token,
        "experiment_name": experiment_name,
        "run_name": run_name
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_runs_by_experiment(hostname, token, experiment_name, experiment_group_name=None, dare_platform=None, kind=None):
    url = hostname + "/runs/byexp"
    data = {
        "access_token": token,
        "experiment_name": experiment_name,
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform
    if kind:
        data["kind"] = kind

    response = requests.get(url, params=data)
    return response.status_code, response.text


def get_run_by_name(hostname, token, experiment_name, run_name, experiment_group_name=None, dare_platform=None):
    url = hostname + "/runs/byname"

    data = {
        "access_token": token,
        "experiment_name": experiment_name,
        "run_name": run_name
    }

    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    if dare_platform:
        data["dare_platform"] = dare_platform

    response = requests.get(url, params=data)
    return response.status_code, response.text


def get_runs_by_user(hostname, token):
    url = hostname + "/runs/byuser"

    data = {"access_token": token}

    response = requests.get(url, params=data)
    return response.status_code, response.text


# ************************** Upload ****************************************
def create_upload(hostname, token, filename, path_to_file, remote_path=None, dare_platform=None, dataset_name=None):
    url = hostname + "/uploads/"

    data = {
        "access_token": token,
        "filename": filename
    }

    if dataset_name:
        data["dataset_name"] = dataset_name
    if remote_path:
        data["path"] = remote_path
    if dare_platform:
        data["dare_platform"] = dare_platform

    with open(join(path_to_file, filename), 'rb') as s:
        script = s.read()
    files = {filename: script}
    data["files"] = files

    response = requests.post(url, json=data)
    return response.status_code, response.text


def update_upload(hostname, token, old_filename, old_path=None, new_filename=None, new_folder_name=None,
                  path_to_file=None, dare_platform=None):
    url = hostname + "/uploads/update-upload/"

    data = {
        "access_token": token,
        "filename": old_filename
    }

    if old_path:
        data["folder"] = old_path
    if dare_platform:
        data["dare_platform"] = dare_platform
    if new_filename:
        data["new_filename"] = new_filename
    if new_folder_name:
        data["new_folder"] = new_folder_name
    if new_filename:
        with open(join(path_to_file, new_filename), 'rb') as s:
            script = s.read()
        files = {new_filename: script}
        data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def delete_upload(hostname, token, filename, folder, dare_platform=None):
    url = hostname + "/uploads/delete-upload/"

    data = {
        "access_token": token,
        "filename": filename
    }

    if folder:
        data["folder"] = folder
    if dare_platform:
        data["dare_platform"] = dare_platform
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_upload_by_user(hostname, token, dare_platform=None):
    url = hostname + "/uploads/byuser"

    params = {
        "access_token": token
    }

    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


def get_upload_by_folder(hostname, token, folder, dare_platform=None):
    url = hostname + "/uploads/byfolder"

    params = {
        "access_token": token,
        "folder": folder
    }

    if dare_platform:
        params["dare_platform"] = dare_platform

    response = requests.get(url, params=params)
    return response.status_code, response.text


# ************************ Download ****************************************
def get_full_path(hostname, token, kind, experiment_name=None, experiment_group_name=None, run_name=None,
                  upload_folder=None, upload_file=None):
    url = hostname + "/downloads/fullpath"
    data = {
        "access_token": token,
        "kind": kind
    }
    if kind == "experiments":
        if experiment_name:
            data["experiment_name"] = experiment_name
        if experiment_group_name:
            data["experiment_group_name"] = experiment_group_name
        if run_name:
            data["run_name"] = run_name
    elif kind == "uploads":
        if upload_folder:
            data["folder"] = upload_folder
        if upload_file:
            data["filename"] = upload_file
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_file(hostname, token, experiment_name, run_name, filename, experiment_group_name=None):
    url = hostname + "/downloads/download"
    data = {
        "access_token": token,
        "experiment_name": experiment_name,
        "run_name": run_name,
        "filename": filename
    }
    if experiment_group_name:
        data["experiment_group_name"] = experiment_group_name
    try:
        myfile = requests.get(url, params=data)
        with open(filename, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(filename)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)
